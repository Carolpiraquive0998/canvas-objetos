function inicializar() {

    let simulador = document.getElementById('simulador'); 
    let width=50;
    let height=70;
    simulador.width = window.innerWidth;
    simulador.height = window.innerHeight;
    ctx = simulador.getContext('2d');
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.strokeStyle = "orange"
    ctx.moveTo(0, (window.innerHeight / 2)-100);
    ctx.lineTo(window.innerWidth, (window.innerHeight / 2)-100);
    ctx.stroke();
    ctx.translate(0, window.innerHeight/2);
    ctx.scale(2, -2);
}

function dibujarrectangulo() {
    var canvas = document.getElementById('canvas');
    if (canvas.getContext) {
      var ctx = canvas.getContext('2d');
    ctx.beginPath();
    ctx.fillRect(5,5,10,10);
    ctx.strokeRect(2,2,5,5);
    ctx.fillStyle="green";
  }
}


function dibujarCirculo(x, y, radio) {
    ctx.beginPath();
    ctx.arc(x, y, radio, 0, 2 * Math.PI, true);
    ctx.fill();
    ctx.fillStyle='blue';
    

}

function dibujarTrigonometrica(opcion, frecuencia) {
    let x;
    let desplazamiento = 300;
    let amplitud = 300;
    for (x = 0; x < 360 * 10; x += (1 / frecuencia)) {

        if (opcion == '0') {

            y = Math.sin(x * frecuencia * Math.PI / 180) * amplitud + desplazamiento;

        } else {
            y = Math.cos(x * frecuencia * Math.PI / 180) * amplitud + desplazamiento;
        }
        dibujarCirculo(x, y, 1);
    }

}

                                                                                                                                                                                                                             
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
function simular(){

    let t=0, y=0, x=0;
    let Vo=document.getElementById('Vo').value;
    let Wo=document.getElementById('Wo').value;
    Wo=(Wo*Math.PI)/180;
    while(y>=0){
    Vox=Vo*Math.cos(Wo);
        x=Vox*t;
        y=(Vo*Math.sin(Wo)*t)-((0.5)*9.8*t*t);
        console.log("x="+x+"y="+y+"t="+t+" rad"+Wo);
        t+=.02;
        dibujarCirculo(x, y, 1);
        

    }


}